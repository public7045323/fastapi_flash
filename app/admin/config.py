from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from app.config.config import settings
from app.users.models import UserORM
from app.ext.servers.models import Server_Base
from app.ext.info.models import TableRCU, MedCentrs, Contact
from app.ext.switches.models import SwitchORM, SwitchErrorORM




DATABASE_URL = settings.DATABASE_URL
DATABASE_PARAMS_USERS = {}
engine_main_base = create_async_engine(DATABASE_URL, **DATABASE_PARAMS_USERS)

engine_servers_base = create_async_engine(settings.DATABASE_URL_SERVERS_BASE)
engine_script_info = create_async_engine(settings.DATABASE_URL_SCRIPT_INFO)
engine_switches = create_async_engine(settings.DATABASE_URL_SWITCHES)


SessionAdmin = async_sessionmaker()
SessionAdmin.configure(binds={
    UserORM: engine_main_base,
    TableRCU: engine_main_base,
    MedCentrs: engine_main_base,
    Server_Base: engine_servers_base,
    Contact: engine_script_info, 
    SwitchORM: engine_switches,
    SwitchErrorORM: engine_switches,
    }
)