import asyncio
from sqladmin import ModelView
from fastapi import Depends, Request

from app.users.dependencies import get_current_user
from app.users.models import UserORM
from app.ext.servers.models import Server_Base
from app.ext.info.models import TableRCU, MedCentrs, Contact
from app.ext.switches.models import SwitchORM, SwitchErrorORM





class UserAdmin(ModelView, model=UserORM):
    async def is_accessible(self, request = Request) -> bool:
        request.session["token"]
        user = await get_current_user(request.session["token"])
        if user.is_admin or "users_crud" in user.access.split(", "):
            return True
        else:
            return False


    # column_details_exclude_list=[User.hashed_password]
    name = 'Пользователь'
    name_plural = 'Пользователи'
    icon = 'fa-solid fa-users'
    page_size = 20
    page_size_options = [10, 25, 50, 100]
    can_delete = True
    column_default_sort = ("id", True)
    column_searchable_list = [UserORM.name, UserORM.email]

    column_list = [
        UserORM.id, 
        UserORM.name, 
        UserORM.email, 
        UserORM.access,
        UserORM.is_active,
        UserORM.is_admin, 
        UserORM.is_sqladmin,
        UserORM.tz,
        UserORM.entered_at,
        UserORM.registered_at
    ]
    column_sortable_list = [
        UserORM.id,
        UserORM.name,
        UserORM.email,
        UserORM.access,
        UserORM.is_active,
        UserORM.is_admin,
        UserORM.is_sqladmin,
        UserORM.tz,
        UserORM.entered_at,
        UserORM.registered_at
    ]
    column_labels = {
        UserORM.id: "#",
        UserORM.name: "Имя", 
        UserORM.email: "Почта",
        UserORM.access: 'Доступ',
        UserORM.is_active: 'Активен',
        UserORM.is_admin: 'Админ',
        UserORM.is_sqladmin: 'SQL Админ',
        UserORM.tz: 'Часовй пояс',
        UserORM.entered_at: 'Вход',
        UserORM.registered_at: 'Зарегестрирован'
    }
    can_export = False


class ServerBaseAdmin(ModelView, model=Server_Base):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "server_base_crud" in user.access.split(", "):
            return True
        else:
            return False
    column_list = [
        Server_Base.id, 
        Server_Base.hypervisor, 
        Server_Base.virtual, 
        Server_Base.root_id,
        Server_Base.name,
        Server_Base.ip, 
        Server_Base.ipmi, 
        Server_Base.link, 
        Server_Base.address,
        Server_Base.description,
        Server_Base.created_on,
        Server_Base.updated_on,
    ]
    can_delete = True
    name = 'База серверов'
    name_plural = 'База серверов'
    icon = 'fa-solid fa-layer-group'
    page_size = 12
    page_size_options = [10, 25, 50, 100]
    column_searchable_list = [Server_Base.name, Server_Base.ip, Server_Base.link]
    column_sortable_list = [
        Server_Base.id, 
        Server_Base.hypervisor, 
        Server_Base.virtual, 
        Server_Base.root_id,
        Server_Base.name,
        Server_Base.ip, 
        Server_Base.ipmi, 
        Server_Base.link, 
        Server_Base.address,
        Server_Base.description,
        Server_Base.created_on,
        Server_Base.updated_on,
    ]
    column_default_sort = ("id", True)
    column_labels = {
        Server_Base.id: '#', 
        # Server_Base.hypervisor, 
        # Server_Base.virtual, 
        # Server_Base.root_id,
        # Server_Base.name,
        # Server_Base.ip, 
        # Server_Base.ipmi, 
        # Server_Base.link, 
        # Server_Base.address,
        # Server_Base.description,
        Server_Base.created_on: 'created_at',
        Server_Base.updated_on: 'updated_at',
    }
    can_export = False

################################################################

class TableRCUAdmin(ModelView, model=TableRCU):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "info_scripts_crud" in user.access.split(", "):
            return True
        else:
            return False

    column_list = [c.name for c in TableRCU.__table__.c]
    column_searchable_list = [TableRCU.id, TableRCU.city]
    column_default_sort = ("id", True)
    column_formatters = {TableRCU.by_switches: lambda l, a: l.by_switches[:70]}
    name = 'Таблица РЦУ'
    name_plural = 'Таблица РЦУ'
    icon = 'fa-solid fa-address-book'
    page_size = 12
    page_size_options = [10, 25, 50, 100]

    column_sortable_list = [
        TableRCU.id,
        TableRCU.city,
        TableRCU.etc,
        TableRCU.city_tag,
        TableRCU.by_switches,
        TableRCU.boss_ETC,
        TableRCU.lead_engineer,
    ]

    column_labels = {
        TableRCU.id: '#',
        TableRCU.city: 'Город',
        TableRCU.etc: 'ЭТЦ',
        TableRCU.city_tag: 'Тэг',
        TableRCU.by_switches: 'По коммутаторам',
        TableRCU.boss_ETC: 'Начальник ЭТЦ',
        TableRCU.lead_engineer: 'Ведущий инженер',
    }

###############################################################

class MedCentrsAdmin(ModelView, model=MedCentrs):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "info_scripts_crud" in user.access.split(", "):
            return True
        else:
            return False

    column_list = [c.name for c in MedCentrs.__table__.c]
    column_searchable_list = [MedCentrs.id, MedCentrs.name]
    column_default_sort = ("id", True)
    # column_formatters = {MedCentrs.address: lambda l, a: l.address[:70]}
    name = 'МедЦентр'
    name_plural = 'МедЦентры'
    icon = 'fa-solid fa-heart'
    page_size = 12
    page_size_options = [10, 25, 50, 100]

    column_sortable_list = [
        MedCentrs.id,
        MedCentrs.name,
        MedCentrs.address,
        MedCentrs.provider,
        MedCentrs.ip,
        MedCentrs.note,
    ]

    column_labels = {
        MedCentrs.id: '#',
        MedCentrs.name: 'Наименовае объекта',
        MedCentrs.address: 'Адрес',
        MedCentrs.provider: 'Провайдер',
        MedCentrs.ip: 'IP-адрес',
        MedCentrs.note: 'Примечание',
    }

##########################################################################

class ContactAdmin(ModelView, model=Contact):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin or "info_scripts_crud" in user.access.split(", "):
            return True
        else:
            return False

    column_list = [c.name for c in Contact.__table__.c]
    column_searchable_list = [Contact.id, Contact.name, Contact.link]
    column_default_sort = ("id", True)
    # column_formatters = {Contact.address: lambda l, a: l.address[:70]}
    name = 'Контакт'
    name_plural = 'Контакты'
    icon = 'fa-solid fa-comments'
    page_size = 12
    page_size_options = [10, 25, 50, 100]

    column_sortable_list = [
        Contact.id,
        Contact.name,
        Contact.contact,
        Contact.link,
        Contact.address,
        Contact.email,
        Contact.created_at,
        Contact.updated_at,
    ]

    column_labels = {
        Contact.id: '#',
        Contact.name: 'Провайдер',
        Contact.contact: 'Контакт',
        Contact.link: 'Стык',
        Contact.address: 'Адрес',
        Contact.email: 'Почта',
        Contact.created_at: 'Создано',
        Contact.updated_at: 'Обновлено',
    }

###################################################################################

class SwitchAdmin(ModelView, model=SwitchORM):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin:
            return True
        else:
            return False
    
    column_list = [c.name for c in SwitchORM.__table__.c] 
    column_default_sort = ("id", True)
    column_searchable_list = [SwitchORM.name, SwitchORM.ip]
    column_formatters = {SwitchORM.model: lambda l, a: l.model[:50] if l.model else '-'}
    page_size = 12
    name = 'Свитч'
    name_plural = 'Свитчи'
    icon = 'fa-solid fa-layer-group'

    column_sortable_list = [
        SwitchORM.id,
        SwitchORM.name, 
        SwitchORM.ip,
        SwitchORM.status,
        SwitchORM.model,
        SwitchORM.mac,
        SwitchORM.fw,
        SwitchORM.date_status,
        SwitchORM.date_cu,
    ]

    column_labels = {
        SwitchORM.id: '#',
        SwitchORM.name: 'Название', 
        SwitchORM.ip: 'IP-адрес',
        SwitchORM.status: 'Статус',
        SwitchORM.model: 'Модель',
        SwitchORM.mac: 'Мак-адрес',
        SwitchORM.fw: 'Прошивка',
        SwitchORM.date_status: 'Дата статуса',
        SwitchORM.date_cu: 'Дата добавления\ обновления',
    }

#################################################################

class SwitchErrorAdmin(ModelView, model=SwitchErrorORM):
    async def is_accessible(self, request = Request) -> bool:
        user = await get_current_user(request.session["token"])
        if user.is_admin:
            return True
        else:
            return False
    
    column_list = [c.name for c in SwitchErrorORM.__table__.c] + [SwitchErrorORM.switch]
    # column_exclude_list = [SwitchErrorORM.switch_pk]
    column_default_sort = ("id", True)
    column_searchable_list = [SwitchErrorORM.id, SwitchErrorORM.switch_pk]
    # column_formatters = {SwitchORM.model: lambda l, a: l.model[:50] if l.model else '-'}
    page_size = 12
    name = 'Свитч Ошибки'
    name_plural = 'Свитчи Ошибки'
    icon = 'fa-solid fa-layer-group'

    column_labels = {
        SwitchErrorORM.id: '#',
        SwitchErrorORM.port: 'Порт',
        SwitchErrorORM.error1: 'Error 1', 
        SwitchErrorORM.error2: 'Error 2',
        SwitchErrorORM.date1: 'Дата 1',
        SwitchErrorORM.date2: 'Дата 2',
        SwitchErrorORM.switch_pk: 'ИД оборудования',
        SwitchErrorORM.switch: 'Оборудование',
    }

    column_sortable_list = [
        SwitchErrorORM.id,
        SwitchErrorORM.port,
        SwitchErrorORM.error1, 
        SwitchErrorORM.error2,
        SwitchErrorORM.date1,
        SwitchErrorORM.date2,
        SwitchErrorORM.switch_pk,
        SwitchErrorORM.switch,
    ]
