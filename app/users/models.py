from typing import Annotated
from sqlalchemy import Column, DateTime, Integer, String, Boolean, TIMESTAMP, text
from sqlalchemy.orm import relationship, Mapped, mapped_column
from sqlalchemy.types import Text
from sqlalchemy.sql import func
from app.config.database import Base
from datetime import datetime


class UserORM(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(Text)
    email: Mapped[str] = mapped_column(Text, unique=True)
    hashed_password: Mapped[str] = mapped_column(Text)
    access: Mapped[str|None] = mapped_column(Text)
    is_active: Mapped[bool | None] = mapped_column(default=False)
    is_admin: Mapped[bool | None] = mapped_column(default=False)
    is_sqladmin: Mapped[bool | None] = mapped_column(default=False)
    tz: Mapped[float | None] = mapped_column(default=7)
    entered_at: Mapped[datetime | None]
    registered_at: Mapped[datetime | None] = mapped_column(onupdate=datetime.utcnow())


    def __str__(self):
        return f'User: {self.email}'
