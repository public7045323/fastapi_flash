from datetime import timedelta



def datetime_format_tz(value, tz=7, format='%d.%m.%Y %H:%M:%S'):
    value_tz = value + timedelta(hours=tz)
    return value_tz.strftime(format)