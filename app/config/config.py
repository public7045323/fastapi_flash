from typing import Literal
from pydantic import root_validator
from pydantic_settings import BaseSettings



class Settings(BaseSettings):
    MODE: Literal['DEV', 'TEST', 'PROD']

    TOKEN: str
    WWW: str

    DB_HOST: str
    DB_PORT: int
    DB_USER: str
    DB_PASS: str
    DB_NAME: str

    @property
    def DATABASE_URL(self):
        return f"postgresql+asyncpg://{self.DB_USER}:{self.DB_PASS}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"
    
    DB_HOST_SWITCHES: str
    DB_PORT_SWITCHES: int
    DB_USER_SWITCHES: str
    DB_PASS_SWITCHES: str
    DB_NAME_SWITCHES: str

    @property
    def DATABASE_URL_SWITCHES(self):
        return f"postgresql+asyncpg://{self.DB_USER_SWITCHES}:{self.DB_PASS_SWITCHES}@{self.DB_HOST_SWITCHES}:{self.DB_PORT_SWITCHES}/{self.DB_NAME_SWITCHES}"
    
    DB_HOST_SERVERS_BASE: str
    DB_PORT_SERVERS_BASE: int
    DB_USER_SERVERS_BASE: str
    DB_PASS_SERVERS_BASE: str
    DB_NAME_SERVERS_BASE: str

    @property
    def DATABASE_URL_SERVERS_BASE(self):
        return f"postgresql+asyncpg://{self.DB_USER_SERVERS_BASE}:{self.DB_PASS_SERVERS_BASE}@{self.DB_HOST_SERVERS_BASE}:{self.DB_PORT_SERVERS_BASE}/{self.DB_NAME_SERVERS_BASE}"
    
    DB_HOST_SCRIPT: str
    DB_PORT_SCRIPT: int
    DB_USER_SCRIPT: str
    DB_PASS_SCRIPT: str
    DB_NAME_SCRIPT: str

    @property
    def DATABASE_URL_SCRIPT_INFO(self):
        return f"postgresql+asyncpg://{self.DB_USER_SCRIPT}:{self.DB_PASS_SCRIPT}@{self.DB_HOST_SCRIPT}:{self.DB_PORT_SCRIPT}/{self.DB_NAME_SCRIPT}"

    SECRET_KEY: str
    ALGORITHM: str

    REDIS_HOST: str
    REDIS_PORT: str

    SMTP_HOST: str
    SMTP_PORT: int
    SMTP_USER: str
    SMTP_PASS: str
    
    class Config:
        env_file = '.env'


settings = Settings()
