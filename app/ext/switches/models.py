from typing import Annotated
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, TIMESTAMP, text, BIGINT
from sqlalchemy.orm import relationship, Mapped, mapped_column
from sqlalchemy.types import Text
from sqlalchemy.sql import func
from app.config.database import Base
from datetime import datetime, timezone



created_at = Annotated[datetime, mapped_column(server_default=text('now()'))]

updated_at = Annotated[datetime, mapped_column(
        onupdate=datetime.now,
        nullable=True
    )]



class SwitchORM(Base):
    __tablename__ = "switches"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    ip: Mapped[str] = mapped_column(unique=True)
    status: Mapped[bool | None]
    model: Mapped[str | None] = mapped_column(Text)
    mac: Mapped[str | None]
    fw: Mapped[str | None]

    errors_ext: Mapped[list["SwitchErrorORM"]] = relationship(
        back_populates="switch",
        uselist=True,
    )

    date_status: Mapped[datetime | None]
    date_cu: Mapped[datetime | None] 

    def __str__(self):
        return f"Switch: id={self.id}, name={self.name}, ip={self.ip}"
    
    def __repr__(self) -> str:
        return str(self)

####################################################################################################

class SwitchErrorORM(Base):
    __tablename__ = "switches_errors"

    id: Mapped[int] = mapped_column(primary_key=True)
    port: Mapped[str | None]
    error1: Mapped[int | None] = mapped_column(BIGINT)
    error2: Mapped[int | None] = mapped_column(BIGINT)
    date1: Mapped[datetime | None] 
    date2: Mapped[datetime | None]

    switch_pk: Mapped[int] = mapped_column(ForeignKey("switches.id", ondelete="CASCADE"))
    switch: Mapped["SwitchORM"] = relationship(
        back_populates="errors_ext",
        lazy='selectin',
        uselist=False
        )

    def __str__(self):
        return f"SwitchError: id={self.id}, port={self.port}, error1={self.error1}, error2={self.error2}, switch_pk={self.switch_pk}"
    
    def __repr__(self) -> str:
        return str(self)

########################################################################################################