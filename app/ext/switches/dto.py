import math
from sqlalchemy import and_, desc, func, or_, select
from sqlalchemy.orm import selectinload
from app.config.dto.base import BaseDTO
from app.ext.switches.models import SwitchORM, SwitchErrorORM
from app.config.database import async_session_maker_switches



class SwitchesDTO(BaseDTO):
    async_session_maker = async_session_maker_switches
    model = SwitchORM
    model2 = SwitchErrorORM

    # Find all switches contrast errors.
    @classmethod
    async def findErrorContrastAll(cls) -> list:
        async with cls.async_session_maker() as session:
            stmt = select(cls.model2).where(cls.model2.error1 != cls.model2.error2).order_by(cls.model2.id)
            result = await session.execute(stmt)
            result = result.scalars().all()

            return result
    
    # Find all switches.
    @classmethod
    async def findSwichAll(cls) -> list:
        async with cls.async_session_maker() as session:
            stmt = select(cls.model).order_by(cls.model.date_cu.desc())
            result = await session.execute(stmt)
            result = result.scalars().all()

            return result

    @classmethod
    async def paginateSwitches(cls, page, size):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).order_by(cls.model.__table__.columns.date_cu.desc()).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
                        'content': data,
                        'page_number': page,
                        'page_size': size,
                        'total_pages': total_pages,
                        'total_records': total_records
                        }
            
            return paginate
    
    @classmethod
    async def paginateFindSwitch(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                cls.model.__table__.columns.model.ilike(f'%{q}%'),
                )
            ).order_by(cls.model.__table__.columns.name).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                cls.model.__table__.columns.model.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
                        'content': data,
                        'page_number': page,
                        'page_size': size,
                        'total_pages': total_pages,
                        'total_records': total_records,
                        'q': q,
                        }
            
            return paginate
    
    @classmethod
    async def paginateSwitchError(cls, page, size):
        async with cls.async_session_maker() as session:
            query = (select(cls.model2).options(
                    selectinload(cls.model2.switch))
                    .where(
                        and_(cls.model2.__table__.columns.date2 != None,
                            cls.model2.__table__.columns.error1 != cls.model2.__table__.columns.error2)
                            )
                    .order_by(cls.model2.__table__.columns.date1.desc())
                    .offset((page-1)*size)
                    .limit(size)
                    )
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = (select(func.count())
                        .select_from(cls.model2.__table__)
                        .where(
                            and_(cls.model2.__table__.columns.date2 != None,
                                cls.model2.__table__.columns.error1 != cls.model2.__table__.columns.error2)
                            )
                        )
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
                        'content': data,
                        'page_number': page,
                        'page_size': size,
                        'total_pages': total_pages,
                        'total_records': total_records
                        }
            
            return paginate
        
    @classmethod
    async def paginateFindSwitchError(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = (select(cls.model2).join((cls.model))
                    .where(
                        and_(
                            cls.model2.date2 != None,
                            # cls.model2.error1 != cls.model2.error2
                            ),
                        or_(
                            cls.model.name.ilike(f'%{q}%'),
                            cls.model.ip.ilike(f'%{q}%'),
                            )
                    )
                    .offset((page-1)*size).limit(size))

            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
                        
            query_all = (select(func.count()).select_from(cls.model2).join((cls.model))
                    .where(
                        and_(
                            cls.model2.date2 != None,
                            # cls.model2.error1 != cls.model2.error2
                            ),
                        or_(
                            cls.model.name.ilike(f'%{q}%'),
                            cls.model.ip.ilike(f'%{q}%'),
                            )
                    )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0

            total_pages = math.ceil(total_records/size)
            
            paginate = {
                        'content': data,
                        'page_number': page,
                        'page_size': size,
                        'total_pages': total_pages,
                        'total_records': total_records,
                        'q': q,
                        }
            
            return paginate
