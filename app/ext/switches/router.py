from typing import Annotated
from fastapi import APIRouter, Depends, Request, Response, Path
from fastapi.params import Query
from fastapi.responses import FileResponse, HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from app.config.flash import flash, get_flashed_messages
from app.users.dependencies import get_current_user
from app.users.models import UserORM
from app.config.access import site_access
from app.config import jinja_filters
from app.ext.switches.dto import SwitchesDTO



router = APIRouter( prefix='/switches', tags=['Switches'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages
templates.env.filters["my_date_filter"] = jinja_filters.datetime_format_tz


@router.get('/error_json')
async def getErrorJson(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
):
    access = await site_access(request, current_user, 'switches_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))

    data_error_all_base = await SwitchesDTO.findErrorContrastAll()

    data_json = {}
    for item in data_error_all_base:
        data_json[item.id] = {
                            'name': item.switch.name,
                            'ip': item.switch.ip,
                            'port': item.port,
                            'error1': item.error1,
                            'error2': item.error2,
                            'date1': item.date1,
                            'date2': item.date2,
                            }
    return data_json

####################################################################################

@router.get('/all_json')
async def getSwitchAllJson(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
):
    access = await site_access(request, current_user, 'switches_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))

    data_sw_all = await SwitchesDTO.findSwichAll()

    data_json = {}
    for item in data_sw_all:
        data_json[item.id] = {
                            'name': item.name,
                            'ip': item.ip,
                            'status': item.status,
                            'model': item.model,
                            'mac': item.mac,
                            }
    return data_json

####################################################################################

@router.get('/all')
async def getSwitchesAll(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=17),
):
    access = await site_access(request, current_user, 'switches_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_main= {'title': 'Коммутаторы'}

    paginate = await SwitchesDTO.paginateSwitches(page, size)

    if paginate is None:
        flash(request, 'Нет данных.', 'alert-info')
    
    return templates.TemplateResponse(
        "switches/switches_all.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

####################################################################################

@router.get('/search_sw/')
async def getSearchSwitch(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=17),
    q: str | None = Query(default=None), 
):
    access = await site_access(request, current_user, 'switches_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_main = {'title': 'Поиск по Коммутаторам'}
    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('getSwitchesAll'), status_code=303)
    
    paginate = await SwitchesDTO.paginateFindSwitch(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('getSwitchesAll'), status_code=303)
    
    return templates.TemplateResponse(
        "switches/switches_all.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

###########################################################################################################

@router.get('/error')
async def getSwitchError(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=17),
):
    access = await site_access(request, current_user, 'switches_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_main= {'title': 'Ошибки на Ком'}

    paginate = await SwitchesDTO.paginateSwitchError(page, size)

    if paginate is None:
        flash(request, 'Нет данных.', 'alert-info')
    
    return templates.TemplateResponse(
        "switches/switches_error.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

##########################################################################################

@router.get('/search_error/')
async def getSearchSwitchError(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=17),
    q: str | None = Query(default=None), 
):
    access = await site_access(request, current_user, 'switches_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_main = {'title': 'Поиск по ошибкам на коммутаторах'}

    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('getSwitchError'), status_code=303)
    
    paginate = await SwitchesDTO.paginateFindSwitchError(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('getSwitchError'), status_code=303)
    
    return templates.TemplateResponse(
        "switches/switches_error.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )