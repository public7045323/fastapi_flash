from typing import Annotated
from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP, text
from sqlalchemy.orm import relationship, Mapped, mapped_column
from sqlalchemy.types import Text
from sqlalchemy.sql import func
from app.config.database import Base
from datetime import datetime



created_at = Annotated[datetime, mapped_column(server_default=text('now()'))]

updated_at = Annotated[datetime, mapped_column(
        onupdate=datetime.now,
        nullable=True
    )]


class TableRCU(Base):
    __tablename__ = "TableRCU"

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    city: Mapped[str] = mapped_column(String(255), unique=True)
    etc: Mapped[int] = mapped_column(nullable=False)
    city_tag: Mapped[str] = mapped_column(String(255))
    by_switches: Mapped[str] = mapped_column(String(500))
    boss_ETC: Mapped[str] = mapped_column(String(255))
    lead_engineer: Mapped[str | None] = mapped_column(String(255))

    def __str__(self):
        return f'Table_RCU: {self.id} - {self.city}'
    
    def __repr__(self) -> str:
        return str(self)

##########################################################################

class MedCentrs(Base):
    __tablename__ = "MedCentrs"

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    name: Mapped[str] = mapped_column(String(255))
    address: Mapped[str | None] = mapped_column(String(255))
    provider: Mapped[str] = mapped_column(String(255))
    ip: Mapped[str | None] = mapped_column(String(255))
    note: Mapped[str | None] = mapped_column(String(500))

    def __str__(self):
        return f'MedCentrs - id: {self.id}, name: {self.name}'

    def __repr__(self) -> str:
        return str(self)

#############################################################################

class Contact(Base):
    __tablename__ = 'messages'
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    name: Mapped[str] = mapped_column(String(255))
    contact: Mapped[str] = mapped_column(String(255))
    link: Mapped[str] = mapped_column(String(255))
    address: Mapped[str] = mapped_column(String(255), nullable=True)
    email: Mapped[str] = mapped_column(String(255), nullable=True)
    created_at: Mapped[created_at]
    updated_at: Mapped[updated_at | None]

    def __str__(self):
        return f'Contact - id: {self.id}, name: {self.name}'

    def __repr__(self) -> str:
        return str(self)