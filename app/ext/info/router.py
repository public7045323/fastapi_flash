from typing import Annotated
from fastapi import APIRouter, Depends, Request, Response, Path
from fastapi.params import Query
from fastapi.responses import FileResponse, HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from app.config.flash import flash, get_flashed_messages
from app.users.dependencies import get_current_user
from app.users.models import UserORM
from app.config.access import site_access
from app.ext.info.dto import TableRCUDTO, MedCentrsDTO, ContactDTO



router = APIRouter( prefix='/info', tags=['Info'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages


@router.get('/table_rcu')
async def table_rcu_get(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=10),

):
    data_main= {'title': 'Таблица РЦУ'}
    
    paginate = await TableRCUDTO.paginate(page, size)
    if paginate is None:
        flash(request, 'Нет данных.', 'alert-info')
    
    return templates.TemplateResponse(
        "info/table_rcu_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

####################################################################################

@router.get('/search_trcu/')
async def table_rcu_search(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=10),
    q: str | None = Query(default=None), 
):
    data_main = {'title': 'Поиск по таблице РЦУ'}
    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('table_rcu_get'), status_code=303)
    
    paginate = await TableRCUDTO.paginate_find(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('table_rcu_get'), status_code=303)
    
    return templates.TemplateResponse(
        "info/table_rcu_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

##################################################################################################

@router.get('/medcentrs')
async def medCentrsGet(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=15),

):
    data_main= {'title': 'МедЦентры'}
    
    paginate = await MedCentrsDTO.paginate(page, size)
    if paginate is None:
        flash(request, 'Нет данных.', 'alert-info')
    
    return templates.TemplateResponse(
        "info/med_centrs/med_centrs_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

######################################################################################################

@router.get('/search_med/')
async def medCentrsSearch(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=10),
    q: str | None = Query(default=None), 
):
    data_main = {'title': 'Поиск по МедЦентрам'}
    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('medCentrsGet'), status_code=303)
    
    paginate = await MedCentrsDTO.paginate_find(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('medCentrsGet'), status_code=303)
    
    return templates.TemplateResponse(
        "info/med_centrs/med_centrs_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

#############################################################################################################

@router.get('/contacts')
async def contactsGet(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=12),

):
    data_main= {'title': 'Контакты'}
    
    paginate = await ContactDTO.paginate(page, size)
    if paginate is None:
        flash(request, 'Нет данных.', 'alert-info')
    
    return templates.TemplateResponse(
        "info/contacts/contacts_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

######################################################################################################

@router.get('/search_ct/')
async def contactsSearch(
    request: Request, 
    current_user: UserORM = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=10),
    q: str | None = Query(default=None), 
):
    data_main = {'title': 'Поиск по МедЦентрам'}
    
    if q:
        q = q.strip()

    else:
        return RedirectResponse(request.url_for('contactsGet'), status_code=303)
    
    paginate = await ContactDTO.paginate_find(page, size, q)
    if not paginate:
        flash(request, f'По вашему запросу *** {q} *** ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('contactsGet'), status_code=303)
    
    return templates.TemplateResponse(
        "info/contacts/contacts_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )
