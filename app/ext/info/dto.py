import math
from app.config.dto.base import BaseDTO
from app.ext.info.models import TableRCU, MedCentrs, Contact
from sqlalchemy import desc, func, or_, select
from app.config.database import async_session_maker_script_info



class TableRCUDTO(BaseDTO):
    model = TableRCU

    @classmethod
    async def paginate(cls, page, size):
        async with cls.async_session_maker() as session:
            query = (
                    select(cls.model.__table__.columns)
                    .order_by(cls.model.__table__.columns.city)
                    .offset((page-1)*size).limit(size)
                    )
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate

    @classmethod
    async def paginate_find(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.city.ilike(f'%{q}%'),
                cls.model.__table__.columns.boss_ETC.ilike(f'%{q}%'),
                cls.model.__table__.columns.by_switches.ilike(f'%{q}%'),
                )
            ).order_by(cls.model.__table__.columns.city).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.city.ilike(f'%{q}%'),
                cls.model.__table__.columns.boss_ETC.ilike(f'%{q}%'),
                cls.model.__table__.columns.by_switches.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records,
            'q': q,
            }
            
            return paginate

####################################################################################

class MedCentrsDTO(BaseDTO):
    model = MedCentrs

    @classmethod
    async def paginate(cls, page, size):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).order_by(cls.model.__table__.columns.name).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate

    @classmethod
    async def paginate_find(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                cls.model.__table__.columns.address.ilike(f'%{q}%'),
                )
            ).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                cls.model.__table__.columns.address.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records,
            'q': q,
            }
            
            return paginate

#######################################################################################

class ContactDTO(BaseDTO):
    model = Contact
    async_session_maker = async_session_maker_script_info

    @classmethod
    async def paginate(cls, page, size):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).order_by(cls.model.__table__.columns.name).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate

    @classmethod
    async def paginate_find(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.contact.ilike(f'%{q}%'),
                cls.model.__table__.columns.link.ilike(f'%{q}%'),
                )
            ).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.contact.ilike(f'%{q}%'),
                cls.model.__table__.columns.link.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records,
            'q': q,
            }
            
            return paginate

