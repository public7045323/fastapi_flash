from typing import Annotated
from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP, text
from sqlalchemy.orm import relationship, Mapped, mapped_column
from sqlalchemy.types import Text
from sqlalchemy.sql import func
from app.config.database import Base
from datetime import datetime



created_at = Annotated[datetime, mapped_column(server_default=text('now()'))]

updated_at = Annotated[datetime, mapped_column(
        onupdate=datetime.now,
        nullable=True
    )]


class Server_Base(Base):
    __tablename__ = "servers_base"
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    hypervisor: Mapped[bool] = mapped_column(nullable=False)
    virtual: Mapped[bool] = mapped_column(nullable=False)
    root_id: Mapped[int] = mapped_column(nullable=False)
    name: Mapped[str] = mapped_column(String(100), nullable=False)
    ip: Mapped[str] = mapped_column(String(100), nullable=False, unique=True)
    ipmi: Mapped[str] = mapped_column(String(100), nullable=True, unique=True)
    link: Mapped[str] = mapped_column(String(200), nullable=False)
    address: Mapped[str] = mapped_column(String(200), nullable=False)
    description: Mapped[str] = mapped_column(String(200), nullable=True)
    created_on: Mapped[created_at]
    updated_on: Mapped[updated_at]

    def __str__(self):
        return f'Server_base: {self.name} - {self.ip}'




