import math
from app.config.dto.base import BaseDTO
from app.config.database import async_session_maker_servers_base
from app.ext.servers.models import Server_Base
from sqlalchemy import desc, func, or_, select



class ServerBaseDTO(BaseDTO):
    model = Server_Base
    async_session_maker = async_session_maker_servers_base

    @classmethod
    async def find_all(cls, **filter_by):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).filter_by(**filter_by).order_by(cls.model.__table__.columns.name)
            result = await session.execute(query)
            return result.all()
        
    @classmethod
    async def find_one_or_none_search(cls, q):
        async with cls.async_session_maker() as session:
            # query = select(cls.model.__table__.columns).filter_by(**filter_by)
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                cls.model.__table__.columns.ipmi.ilike(f'%{q}%'),
                )
            )
            result = await session.execute(query)
            return result.first()
        
    @classmethod
    async def paginate(cls, page, size):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).order_by(cls.model.__table__.columns.name).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate
        
    @classmethod
    async def paginate_find(cls, page, size, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                )
            ).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.ip.ilike(f'%{q}%'),
                )
            )
            
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records,
            'q': q,
            }
            
            return paginate
        



