from typing import Annotated
from fastapi import APIRouter, Depends, Request, Response, Path
from fastapi.params import Query
from fastapi.responses import FileResponse, HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates

from app.config.flash import flash, get_flashed_messages
from app.users.dependencies import get_current_user
from app.users.models import UserORM
from app.config.access import site_access
from app.ext.servers.dto import ServerBaseDTO


import warnings
warnings.filterwarnings('ignore', message='Unverified HTTPS request')



router = APIRouter( prefix='/servers', tags=['Servers'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages


@router.get('/servers_base')
async def servers_base_all_get(
    request: Request, 
    current_user:  UserORM =  Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=1000, default=17),
):
    data_main= {'title': 'База серверов'}
    
    access = await site_access(request, current_user, 'servers_base_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))

    paginate = await ServerBaseDTO.paginate(page, size)
    if paginate is None:
        flash(request, 'Нет данных.', 'alert-info')
    
    return templates.TemplateResponse(
        "servers/servers_base/servers_base_index.html", 
        {"request":request, 
        'user': current_user, 
        'data_main': data_main,
        'paginate': paginate,
        }
    )

########################################################################

@router.get("/servers_base/{server_id}")
async def servers_base_chain_get(
    server_id: Annotated[int, Path(ge=1,)],
    request:Request,
    current_user:  UserORM =  Depends(get_current_user),
):
    data_main = {'title': 'База серверов цепочка'}
    
    access = await site_access(request, current_user, 'servers_base_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_server_base = await ServerBaseDTO.find_by_id(server_id)
    data_virtual_servers = None

    if data_server_base.root_id == 0:
        pass
    
    elif data_server_base.hypervisor:
        data_virtual_servers = await ServerBaseDTO.find_all(root_id = data_server_base.id)
    
    elif data_server_base.virtual:
        data_server_base = await ServerBaseDTO.find_by_id(data_server_base.root_id)
        data_virtual_servers = await ServerBaseDTO.find_all(root_id = data_server_base.id)
    
    return templates.TemplateResponse(
        "servers/servers_base/servers_base_chain.html", 
        {
            "request":request, 
            'user': current_user, 
            'data_main': data_main,
            'data_base': data_server_base,
            'data_virtual_servers': data_virtual_servers
        }
    )

##################################################################################################

@router.get('/search/')
async def servers_base_search(
    request: Request, 
    current_user:  UserORM =  Depends(get_current_user),
    q: str | None = Query(default=None), 
):
    data_main = {'title': 'Поиск по servers base'}
    access = await site_access(request, current_user, 'servers_base_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    if q:
        q = q.strip()
    else:
        return RedirectResponse(request.url_for('wiki_groups_get'), status_code=303)
    
    data_server_base = await ServerBaseDTO.find_one_or_none_search(q)
    data_virtual_servers = None
    
    if data_server_base == None:
        flash(request, f'По вашему запросу: *{q}* ничего не найдено.', 'alert-info')
        return RedirectResponse(request.url_for('servers_base_all_get'), status_code=303)
        

    if data_server_base.root_id == 0:
        pass

    elif data_server_base.hypervisor:
        data_virtual_servers = await ServerBaseDTO.find_all(root_id = data_server_base.id)
    
    elif data_server_base.virtual:
        data_server_base = await ServerBaseDTO.find_by_id(data_server_base.root_id)
        data_virtual_servers = await ServerBaseDTO.find_all(root_id = data_server_base.id)
    
    return templates.TemplateResponse(
        "servers/servers_base/servers_base_chain.html", 
        {
            "request":request, 
            'user': current_user, 
            'data_main': data_main,
            'data_base': data_server_base,
            'data_virtual_servers': data_virtual_servers
        }
    )