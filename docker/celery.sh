#!/bin/bash


if [[ "${1}" == "celery" ]]; then
    celery -A app.celery.celery:celery worker --loglevel=INFO
elif [[ "${1}" == "flower" ]]; then
    celery -A app.celery.celery:celery flower
fi