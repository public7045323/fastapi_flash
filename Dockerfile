FROM python:3.12



RUN mkdir /main

WORKDIR /main

COPY requirements.txt .

RUN apt-get update && apt-get install -y iputils-ping &&  pip install -r requirements.txt 

COPY . .

RUN echo "Asia/Novosibirsk" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

LABEL version="2.0" maintainer="Nikolai Makovenko <n.makovenko@zs.ttk.ru>"

RUN chmod a+x /main/docker/*.sh

CMD ["gunicorn", "my_main:app", "--workers", "4", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind=0.0.0.0:8000"]

